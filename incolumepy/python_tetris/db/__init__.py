import toml
from pathlib import Path

configfile = Path(__file__).parents[3].joinpath("pyproject.toml")
assert configfile.is_file(), f"{configfile=}"

versionfile = Path(__file__).parent.joinpath("version.txt")
assert versionfile.is_file(), f"{versionfile=}"

versionfile.write_text(f"{toml.load(configfile)['tool']['poetry']['version']}\n")
__version__ = versionfile.read_text().strip()


if __name__ == '__main__':
    print(__version__)
