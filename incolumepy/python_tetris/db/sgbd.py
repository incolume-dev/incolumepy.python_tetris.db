from inspect import stack


def pgsql(*args, **kwargs):
    """Postgre SQL"""
    return stack()[0][3], args, kwargs,


def mysql(*args, **kwargs):
    """Mysql."""
    return stack()[0][3], args, kwargs,


def mariadb(*args, **kwargs):
    """Maria DataBase"""
    return stack()[0][3], args, kwargs,


def sqlite(*args, **kwargs):
    """SQLite."""
    return stack()[0][3], args, kwargs,
