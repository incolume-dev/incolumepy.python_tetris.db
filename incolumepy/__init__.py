from pathlib import Path

root = Path(__file__).resolve().parent
try:
    __path__ += [
        root.joinpath("python_tetris").as_posix(),
        root.joinpath("python_tetris", "sgbd").as_posix(),
    ]
except NameError:
    __path__ = [
        root.joinpath("python_tetris").as_posix(),
        root.joinpath("python_tetris", "sgbd").as_posix(),
    ]
