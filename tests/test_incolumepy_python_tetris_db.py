from incolumepy.python_tetris.db import __version__
from incolumepy.python_tetris.db.sgbd import pgsql, mysql, mariadb, sqlite
import pytest
import re


def test_version():
    assert re.fullmatch(r"\d.\d.\d(-\w+.\d)?", __version__, flags=re.I)


@pytest.mark.parametrize(
    ["entrance", "expected"],
    (
        (("a", "1"), ('sqlite', ('a', '1'), {})),
        ((0, 1), ('sqlite', (0, 1), {})),
        (("1", "a"), ('sqlite', ('1', 'a'), {})),
        ((), ('sqlite', (), {})),
    )
)
def test_db_sqlite(entrance, expected):
    assert sqlite(*entrance) == expected
